package fr.openium.mastertp1_2.data.webservice.database

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.openium.mastertp1_2.data.webservice.dao.MemesDao
import fr.openium.mastertp1_2.data.webservice.dto.MemeRespons

@Database(entities = [MemeRespons::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memesDao(): MemesDao
}