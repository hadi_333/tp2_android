package fr.openium.mastertp1_2.data.webservice

object HttpRoutes {
    private const val BASE_URL= "https://api.imgflip.com"
    const val GET_MEMES= "$BASE_URL/get_memes"
}