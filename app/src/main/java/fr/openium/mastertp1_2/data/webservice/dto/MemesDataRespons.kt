package fr.openium.mastertp1_2.data.webservice.dto

data class MemesDataRespons(
    val memes: List<MemeRespons>?
)
