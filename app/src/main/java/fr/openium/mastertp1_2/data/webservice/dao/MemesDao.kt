package fr.openium.mastertp1_2.data.webservice.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import fr.openium.mastertp1_2.data.webservice.dto.MemeRespons

@Dao
interface MemesDao {

    @Query("SELECT * from memes_respons")
    fun getAllMemes(): List<MemeRespons>

    @Insert
    fun insertMeme(vararg meme: MemeRespons)
}

