package fr.openium.mastertp1_2.data.webservice.repository

import com.google.gson.Gson
import fr.openium.mastertp1_2.data.webservice.HttpRoutes
import fr.openium.mastertp1_2.data.webservice.dto.MemeRespons
import fr.openium.mastertp1_2.data.webservice.dto.MemesRespons
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*

class MemesRepository {

    public suspend fun getMemes(): List<MemeRespons>? {
        val client = HttpClient(CIO)
        val respons: HttpResponse = client.get(HttpRoutes.GET_MEMES)
        val responsContent: String = respons.bodyAsText()
        val memesRespons: MemesRespons = Gson().fromJson(responsContent,
            MemesRespons::class.java)

        return memesRespons.data?.memes
    }
}