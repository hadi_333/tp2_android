package fr.openium.mastertp1_2.data.webservice.dto

data class MemesRespons(
    val success: Boolean?,
    val data: MemesDataRespons?
)
