package fr.openium.mastertp1_2.data.webservice.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "memes_respons")
data class MemeRespons(
    @ColumnInfo(name = "meme_id") val id: Int?,
    @ColumnInfo(name = "meme_name") val name: String?,
    @ColumnInfo(name = "meme_url") val url: String?,
    @ColumnInfo(name = "meme_width") val width: Int?,
    @ColumnInfo(name = "meme_height") val height: Int?,
    @ColumnInfo(name = "meme_box_countval") val box_countval: Int?,
    @ColumnInfo(name = "meme_captions") val captions: Int?,
    @PrimaryKey(autoGenerate = true) val uid: Int = 0
)

