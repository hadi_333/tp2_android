package fr.openium.mastertp1_2.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.compose.ui.layout.Layout
import fr.openium.mastertp1_2.data.webservice.dto.MemeRespons
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.openium.mastertp1_2.R


class MemesAdapter(private val memes: List<MemeRespons>) :
    RecyclerView.Adapter<MemesAdapter.MemeViewHolder>() {

        inner class MemeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val textView: TextView = itemView.findViewById(R.id.textView)
            val image: ImageView = itemView.findViewById(R.id.image)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemeViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.meme_item, parent, false)
        return MemeViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MemeViewHolder, position: Int) {
        val meme = memes[position]
        holder.textView.text = meme.name
        Glide.with(holder.image.context).load(meme.url).into(holder.image)
    }

    override fun getItemCount(): Int {
        return memes.size
    }
}



