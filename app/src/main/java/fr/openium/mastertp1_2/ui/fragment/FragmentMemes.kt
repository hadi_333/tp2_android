package fr.openium.mastertp1_2.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import fr.openium.mastertp1_2.R
import fr.openium.mastertp1_2.adapter.MemesAdapter
import fr.openium.mastertp1_2.data.webservice.database.AppDatabase
import fr.openium.mastertp1_2.data.webservice.repository.MemesRepository
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FragmentMemes : Fragment() {
    private lateinit var memesAdapter: MemesAdapter
    private val memesRepository = MemesRepository()
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = Room.databaseBuilder(
            requireContext(),
            AppDatabase::class.java,
            "database-name"
        ).build()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_memes, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewMeme)
        recyclerView.layoutManager = LinearLayoutManager(context)

        memesAdapter = MemesAdapter(listOf())
        recyclerView.adapter = memesAdapter

        GlobalScope.launch {
            try {
                val memesDao = db.memesDao()
                if (memesDao.getAllMemes() == null){
                    val memes = memesRepository.getMemes()
                    if (memes != null) {
                        for (meme in memes) {
                            memesDao.insertMeme(meme)
                        }
                    }
                }
                Log.e("Database", memesDao.getAllMemes().toString())
                val memesFromdb = memesDao.getAllMemes()
                withContext(Dispatchers.Main) {
                    memesAdapter = MemesAdapter(memesFromdb)
                    recyclerView.adapter = memesAdapter
                }
            } catch (e:java.lang.Exception) {
                Log.e("Error", e.toString())
            }
        }

    }
}